﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using LetterWordExercise.Core.Commands;
using LetterWordExercise.Logic.Commands;
using LetterWordExercise.Logic.Parsers;
using LetterWordExercise.Logic.Readers;
using LetterWordExercise.Logic.Tests.Configuration;
using LetterWordExercise.Logic.Tests.Extensions;
using Xunit;

namespace LetterWordExercise.Logic.Tests.Commands
{
  public class GetExtensibleresultHandlerTests : TestBase<GetExtensibleResultHandler, IEnumerable<string>>
  {
    private string _filePath;

    public GetExtensibleresultHandlerTests() : base()
    {
      _filePath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "..\\..\\..\\..\\..\\", "input.txt"));
      var wordParser = new WordParser();
      var fileReader = new FileReader();
      Sut = new GetExtensibleResultHandler(wordParser, fileReader);
    }

    [Theory]
    [InlineData(2)]
    [InlineData(3)]
    [InlineData(4)]
    [InlineData(5)]
    [InlineData(6)]
    [InlineData(7)]
    [InlineData(8)]
    [InlineData(9)]
    [InlineData(10)]
    [InlineData(11)]
    [InlineData(12)]
    public async Task GetExtensibleResult_Returns_List_Of_Strings(int combinationLength)
    {
      // Arrange
      var command = new GetExtensibleResult(combinationLength, _filePath);
      // Act
      Actual = await Sut.Handle(command, default);
      // Assert
      Actual.Should().NotBeNullOrEmpty();
      Actual.All(x => x.Length.Equals((combinationLength * 2) + 2)).Should().BeTrue();
      Actual.Distinct().Count().Should().Be(Actual.Count());
    }

    [Fact]
    public async Task GetExtensibleResult_Throws_When_FilePath_Is_Null()
    {
      // Arrange
      var command = new GetExtensibleResult(Fixture.CreateInt(2, 12), null);
      ExpectedExceptionType = typeof(ArgumentNullException);
      // Act
      ThrownException = SafeTestExecutor.AsyncExecute(async () => Actual = await Sut.Handle(command, default));
      // Assert
      ThrownException.Should().NotBeNull();
      ThrownException.InnerException.Should().BeOfType(ExpectedExceptionType);
      Actual.Should().BeNull();
    }

    [Fact]
    public async Task GetExtensibleResult_Throws_When_FilePath_Is_Not_Found()
    {
      // Arrange
      var command = new GetExtensibleResult(Fixture.CreateInt(2, 12), string.Empty);
      ExpectedExceptionType = typeof(ArgumentException);
      // Act
      ThrownException = SafeTestExecutor.AsyncExecute(async () => Actual = await Sut.Handle(command, default));
      // Assert
      ThrownException.Should().NotBeNull();
      ThrownException.InnerException.Should().BeOfType(ExpectedExceptionType);
      Actual.Should().BeNull();
    }

    [Fact]
    public async Task GetExtensibleResult_Returns_Empty_List_When_No_Results_For_Combination()
    {
      // Arrange
      var command = new GetExtensibleResult(Fixture.CreateInt(50, 100), _filePath);
      // Act
      Actual = await Sut.Handle(command, default);
      // Assert
      Actual.Should().BeEmpty();
    }
  }
}
