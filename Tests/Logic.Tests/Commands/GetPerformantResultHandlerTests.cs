﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using LetterWordExercise.Core.Commands;
using LetterWordExercise.Logic.Commands;
using LetterWordExercise.Logic.Tests.Configuration;
using Xunit;

namespace LetterWordExercise.Logic.Tests.Commands
{
  public class GetPerformantResultHandlerTests : TestBase<GetPerformantResultHandler, IEnumerable<string>>
  {
    public GetPerformantResultHandlerTests() : base()
    {
      Sut = new GetPerformantResultHandler();
    }

    [Fact]
    public async Task GetPerformantResult_Returns_List_Of_Strings_With_Length_Of_6()
    {
      // Arrange
      var command = new GetPerformantResult();
      // Act
      Actual = await Sut.Handle(command, default);
      // Assert
      Actual.Should().NotBeNullOrEmpty();
      Actual.All(x => x.Length.Equals(14)).Should().BeTrue();
      Actual.Distinct().Count().Should().Be(Actual.Count());
    }
  }
}
