﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoFixture;

namespace LetterWordExercise.Logic.Tests.Extensions
{
  public static class AutoFixtureExtensions
  {
    public static int CreateInt(this IFixture fixture, int min, int max)
    {
      return fixture.Create<int>() % (max - min + 1) + min;
    }
  }
}
