﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace LetterWordExercise.Logic.Tests.Configuration
{
    public class SafeTestExecutor
    {
    public static Exception Execute(Action action)
    {
      try
      {
        action();
        return null;
      }
      catch (Exception exception)
      {
        return exception;
      }
    }

    public static Exception AsyncExecute(Func<Task> action)
    {
      Exception exception = null;
      var task = Task.Run(() =>
      {
        try
        {
          action().Wait();
        }
        catch (Exception ex)
        {
          exception = ex;
        }
      });
      var timeout = 5000;
      if (Debugger.IsAttached)
      {
        timeout = 1000000;
      }
      if (!task.Wait(timeout))
      {
        throw new Exception("timeout");
      }
      return exception;
    }
  }
}
