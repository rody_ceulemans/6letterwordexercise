﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoFixture;
using LetterWordExercise.Logic.Tests.Configuration;
using Xunit;

namespace LetterWordExercise.Logic.Tests
{
  [Collection(CollectionNames.TestCollection)]
  public abstract class TestBase<TSut, TResponse>
  {
    protected readonly Fixture Fixture;
    protected Exception ThrownException;
    protected Type ExpectedExceptionType;
    protected TResponse Actual;
    protected TSut Sut;

    protected TestBase()
    {
      Fixture = new Fixture();
    }
  }
}
