﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Threading.Tasks;
using LetterWordExercise.Core.Commands;
using LetterWordExercise.Core.Parsers.Interfaces;
using LetterWordExercise.Core.Readers.Interfaces;
using LetterWordExercise.Logic.Commands;
using LetterWordExercise.Logic.Parsers;
using LetterWordExercise.Logic.Readers;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace ConsoleApp
{
  class Program
  {
    static void Main(string[] args)
    {
      // Setup DI
      var serviceProvider = new ServiceCollection()
        .AddTransient<IParser, WordParser>()
        .AddTransient<IReader, FileReader>()
        .AddMediatR(typeof(GetExtensibleResultHandler).GetTypeInfo().Assembly)
        .BuildServiceProvider();

      var mediator = serviceProvider.GetService<IMediator>();

      // Run the app
      Run(mediator).Wait();
      Console.ReadKey();
    }

    private static async Task Run(IMediator mediator)
    {
      var (maxLength, path) = GetInput();
      var result = await mediator.Send(new GetExtensibleResult(maxLength, path));
      foreach (var word in result)
      {
        Console.WriteLine(word);
      }
    }

    private static Tuple<int, string> GetInput()
    {
      int maxLength;
      var hasMaxLength = false;
      do
      {
        Console.WriteLine("What is the length of the words you want to create?");
        var maxLengthInput = Console.ReadLine();
        if (int.TryParse(maxLengthInput, out maxLength))
        {
          hasMaxLength = true;
        }
      } while (!hasMaxLength);

      string path;
      do
      {
        Console.WriteLine("Please specify the file path:");
        path = Console.ReadLine();
      } while (string.IsNullOrWhiteSpace(path));

      return new Tuple<int, string>(maxLength, path);
    }
  }
}
