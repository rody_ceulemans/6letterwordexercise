﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using LetterWordExercise.Core.Parsers.Interfaces;
using LetterWordExercise.Core.Readers.Interfaces;
using LetterWordExercise.Logic.Commands;
using LetterWordExercise.Logic.Parsers;
using LetterWordExercise.Logic.Readers;
using LetterWordExercise.WinformsApp;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace WinformsApp
{
    static class Program
    {
      private static IServiceProvider ServiceProvider;
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ConfigureServices();
            Application.Run(new Entry(ServiceProvider.GetService<IMediator>()));
        }

        private static void ConfigureServices()
        {
          var services = new ServiceCollection();
          services.AddTransient<IReader, FileReader>();
          services.AddTransient<IParser, WordParser>();
          services.AddMediatR(typeof(GetExtensibleResultHandler).GetTypeInfo().Assembly);
          ServiceProvider = services.BuildServiceProvider();
        }
    }
}
