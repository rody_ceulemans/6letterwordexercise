﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LetterWordExercise.Core.Commands;
using MediatR;

namespace LetterWordExercise.WinformsApp
{
  public partial class Entry : Form
  {
    private const string ButtonName = "GetListBtn";
    private const string TextBoxName = "ResultTxtBox";
    private const string GetListButtonText = "Get List";
    private const string ClearListButtonText = "Clear List";

    private readonly IMediator _mediator;

    public Entry(IMediator mediator)
    {
      _mediator = mediator;

      InitializeComponent();
      WindowState = FormWindowState.Maximized;
      SetButton();
    }

    private void SetButton()
    {
      var newButton = new Button
      {
        Name = ButtonName,
        Text = GetListButtonText,
        Height = 100,
        Width = ClientSize.Width,
        Dock = DockStyle.Top,
        Location = new Point(0, 0),
        BackColor = Color.DarkGray,
        ForeColor = Color.Black
      };
      newButton.Click += HandleGetListClickEvent;
      Controls.Add(newButton);
    }

    private async void HandleGetListClickEvent(object sender, EventArgs e)
    {
      var control = Controls.Find(ButtonName, true).SingleOrDefault();
      if (control is Button button)
      {
        button.Text = ClearListButtonText;
        button.Click -= HandleGetListClickEvent;
        button.Click += HandleClearListClickEvent;
      }

      var txtBox = new TextBox
      {
        Dock = DockStyle.Bottom,
        Location = new Point(0, 100),
        Name = TextBoxName,
        Multiline = true,
        Height = ClientSize.Height - 100,
        Width = ClientSize.Width,
        ScrollBars = ScrollBars.Vertical,
        ReadOnly = true,
        Text = await GetList()
      };
      Controls.Add(txtBox);
    }

    private async Task<string> GetList()
    {
      var list = await _mediator.Send(new GetPerformantResult(), default);
      var stringBuilder = new StringBuilder();
      foreach (var item in list)
      {
        stringBuilder.AppendLine(item);
      }

      return stringBuilder.ToString();
    }

    private void HandleClearListClickEvent(object sender, EventArgs e)
    {
      var txtBoxControl = this.Controls.Find(TextBoxName, true).SingleOrDefault();
      if (txtBoxControl is TextBox textBox)
      {
        Controls.Remove(textBox);
      }

      var btnControl = this.Controls.Find(ButtonName, true).SingleOrDefault();
      if (btnControl is Button button)
      {
        button.Text = GetListButtonText;
        button.Click -= HandleClearListClickEvent;
        button.Click += HandleGetListClickEvent;
      }
    }
  }
}
