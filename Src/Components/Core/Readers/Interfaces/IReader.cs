﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LetterWordExercise.Core.Readers.Interfaces
{
  public interface IReader
  {
    Task<IEnumerable<string>> Read(string path);
  }
}
