﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LetterWordExercise.Core.Parsers.Interfaces
{
  public interface IParser
  {
    Task<IEnumerable<string>> Parse(IEnumerable<string> list, int length);
  }
}
