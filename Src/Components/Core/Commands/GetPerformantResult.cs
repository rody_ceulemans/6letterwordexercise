﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace LetterWordExercise.Core.Commands
{
    public class GetPerformantResult : IRequest<IEnumerable<string>>
    {
      public GetPerformantResult() 
      {
      }
    }
}
