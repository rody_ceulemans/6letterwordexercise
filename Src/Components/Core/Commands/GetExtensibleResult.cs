﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace LetterWordExercise.Core.Commands 
{
  public class GetExtensibleResult : IRequest<IEnumerable<string>>
  {
    public GetExtensibleResult(int combinationLength, string path)
    {
      CombinationLength = combinationLength;
      Path = path;
    }

    public int CombinationLength { get; }
    public string Path { get; } 
  }
}
