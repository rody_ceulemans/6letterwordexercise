﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using LetterWordExercise.Core.Readers.Interfaces;

namespace LetterWordExercise.Logic.Readers
{
  public class FileReader : IReader
  {
    public async Task<IEnumerable<string>> Read(string path)
    {
      return await System.IO.File.ReadAllLinesAsync(path);
    }
  }
}
