﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LetterWordExercise.Core.Parsers.Interfaces;

namespace LetterWordExercise.Logic.Parsers
{
  public class WordParser : IParser
  {
    public async Task<IEnumerable<string>> Parse(IEnumerable<string> list, int length)
    {
      var result = new List<string>();

      foreach (var word in list)
      {
        foreach (var combinableWord in list)
        {
          if (word.Length + combinableWord.Length == length)
          {
            result.Add($"{word}+{combinableWord}={word}{combinableWord}");
          }
        }
      }

      return result.Distinct();
    }
  }
}
