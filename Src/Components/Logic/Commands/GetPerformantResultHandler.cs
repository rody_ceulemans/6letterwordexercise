﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LetterWordExercise.Core.Commands;
using MediatR;

namespace LetterWordExercise.Logic.Commands
{
  public class GetPerformantResultHandler : IRequestHandler<GetPerformantResult, IEnumerable<string>>
  {
    public async Task<IEnumerable<string>> Handle(GetPerformantResult request, CancellationToken cancellationToken)
    {
      var result = new List<string>();

      // read list from file
      var words = await System.IO.File.ReadAllLinesAsync(Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "input.txt")));
      
      //  Create results
      foreach (var word in words)
      {
        foreach (var combinableWord in words)
        {
          if (word.Length + combinableWord.Length == 6)
          {
            result.Add($"{word}+{combinableWord}={word}{combinableWord}");
          }
        }
      }

      return result.Distinct();
    }
  }
}
