﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LetterWordExercise.Core.Commands;
using LetterWordExercise.Core.Parsers.Interfaces;
using LetterWordExercise.Core.Readers.Interfaces;
using LetterWordExercise.Logic.Parsers;
using LetterWordExercise.Logic.Readers;
using MediatR;

namespace LetterWordExercise.Logic.Commands
{
  public class GetExtensibleResultHandler : IRequestHandler<GetExtensibleResult, IEnumerable<string>>
  {
    private readonly IParser _parser;
    private readonly IReader _reader;

    public GetExtensibleResultHandler(IParser wordParser, IReader fileReader)
    {
      _parser = wordParser;
      _reader = fileReader;
    }
    
    public async Task<IEnumerable<string>> Handle(GetExtensibleResult request, CancellationToken cancellationToken)
    {
      var list = await _reader.Read(request.Path);
      return await _parser.Parse(list, request.CombinationLength);
    }
  }
}
